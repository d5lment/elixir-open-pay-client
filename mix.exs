defmodule OpenpayClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :openpay_client,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1.0"},
      {:httpoison, "~> 1.2.0"},
      {:credo, "~> 1.0.5", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.20.2", only: :dev}
      # {:excoveralls, "~> 0.8", only: :test},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
