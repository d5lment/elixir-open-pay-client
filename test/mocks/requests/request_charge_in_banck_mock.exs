defmodule Mocks.Models.Request.RequestChargeInBankMock do
  alias OpenpayClient.Models.Requests.RequestChargeInBank

  @doc """
    Create an existence customer.
  """
  def get_request_charge_in_bank(method, amount, description) do
    %RequestChargeInBank{
      method: method,
      amount: amount,
      description: description
    }
  end
end
