defmodule Mocks.Models.Request.RequestCreateCustomer do
  alias OpenpayClient.Models.Requests.RequestCreateCustomer

  @doc """
    Create basic request to create customer.
  """
  def get_basic_request_create_customer(name, email) do
    %RequestCreateCustomer{
      name: name,
      email: email,
      requires_account: false
    }
  end
end
