defmodule Mocks.Models.CustomerMock do
  alias OpenpayClient.Models.Customer

  @doc """
    Create an existence customer.
  """
  def get_existence_customer do
    %Customer{
      balance: nil,
      clabe: nil,
      creation_date: "2019-04-11T16:02:01-05:00",
      email: "paola@gmail.com",
      external_id: nil,
      id: "afb9huxcqzslqnnshit2",
      last_name: nil,
      name: "customer paola",
      phone_number: nil,
      status: nil
    }
  end
end
