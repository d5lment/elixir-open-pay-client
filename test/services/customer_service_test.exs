defmodule OpenpayClientTest.Services.CustomerServiceTest do
  use ExUnit.Case
  doctest OpenpayClient

  alias Mocks.Models.Request.RequestCreateCustomer
  alias OpenpayClient.Services.CustomerService
  alias OpenpayClient.Models.Error
  alias Mocks.Models.CustomerMock

  test "Successful create new customer" do
    name = "Name"
    email = "name@gmail.com"

    request_create_customer = RequestCreateCustomer.get_basic_request_create_customer(name, email)

    assert {:ok, customer} = CustomerService.create_customer(request_create_customer)
    assert customer.name == name
    assert customer.email == email
    assert customer.id
    assert customer.creation_date
  end

  test "Failure create new customer because nil name" do
    name = nil
    email = "name@gmail.com"

    request_create_customer = RequestCreateCustomer.get_basic_request_create_customer(name, email)

    assert {:error, error} = CustomerService.create_customer(request_create_customer)

    assert error =
             {:error,
              %Error{
                category: "request",
                description: "name no puede estar vacío",
                error_code: 1001,
                http_code: 400
              }}
  end

  test "Get existence customer by id" do
    customer = CustomerMock.get_existence_customer()
    assert CustomerService.get_customer(customer.id) == {:ok, customer}
  end

  test "Get unexistence customer by id" do
    customer_id = "wasawasa"

    _customer = CustomerService.get_customer(customer_id)

    assert customer =
             {:error,
              %Error{
                category: "request",
                description: "The customer with id 'wasawasa' does not exist",
                error_code: 1005,
                http_code: 404
              }}
  end
end
