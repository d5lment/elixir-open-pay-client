defmodule OpenpayClientTest.Services.ChargeServiceTest do
  use ExUnit.Case
  doctest OpenpayClient

  alias OpenpayClient.Models.Transaction
  alias OpenpayClient.Models.Error
  alias Mocks.Models.CustomerMock
  alias Mocks.Models.Request.RequestChargeInBankMock
  alias OpenpayClient.Services.ChargeService

  test "Successfull create charge in bank with spei" do
    customer = CustomerMock.get_existence_customer()
    customer_id = customer.id
    amount = "222"
    method = "bank_account"
    description = "Successfull create charge in bank with spei"

    request_charge_in_bank =
      RequestChargeInBankMock.get_request_charge_in_bank(method, amount, description)

    assert {
             :ok,
             %Transaction{
               amount: amount,
               currency: "MXN",
               customer_id: customer_id,
               method: method,
               status: "in_progress",
               transaction_type: "charge",
               description: description
             } = transaction
           } = ChargeService.create_charge_in_bank(customer_id, request_charge_in_bank)

    assert transaction.id
  end

  test "Failure create charge in bank with spei for unexistence customer" do
    customer_id = "wasawasa"
    amount = "222"
    method = "bank_account"
    description = "Successfull create charge in bank with spei"

    request_charge_in_bank =
      RequestChargeInBankMock.get_request_charge_in_bank(method, amount, description)

    _transaction = ChargeService.create_charge_in_bank(customer_id, request_charge_in_bank)

    assert customer =
             {:error,
              %Error{
                category: "request",
                description: "The customer with id '#{customer_id}' does not exist",
                error_code: 1005,
                http_code: 404
              }}
  end
end
