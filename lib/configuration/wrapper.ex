defmodule OpenpayClient.Wrapper do
  @moduledoc """
    Wrapping using HTTPoison.Base
    * https://github.com/edgurgel/httpoison
  """
  use HTTPoison.Base

  @openpay_cfg Application.get_env(:openpay_client, :configuration)

  def process_url(path) do
    @openpay_cfg[:base_url] <> path
  end
end
