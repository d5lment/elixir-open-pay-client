defmodule OpenpayClient.Models.Transaction do
  @moduledoc """
    Error definition from openpay api docs https://www.openpay.mx/docs/api/#objeto-transacci-n
  """

  defstruct [
    :id,
    :authorization,
    :transaction_type,
    :operation_type,
    :currency,
    :method,
    :creation_date,
    :order_id,
    :status,
    :amount,
    :description,
    :error_message,
    :customer_id,
    :bank_account
  ]
end
