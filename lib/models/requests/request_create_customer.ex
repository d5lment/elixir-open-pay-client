defmodule OpenpayClient.Models.Requests.RequestCreateCustomer do
  @moduledoc """
    Customer definition from openpay api docs https://www.openpay.mx/docs/api/#crear-un-nuevo-cliente
  """
  @enforce_keys [:name, :email]

  defstruct [:external_id, :name, :last_name, :email, :requires_account, :phone_number, :address]
end
