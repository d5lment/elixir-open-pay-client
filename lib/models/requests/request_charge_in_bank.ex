defmodule OpenpayClient.Models.Requests.RequestChargeInBank do
  @moduledoc """
    Customer definition from openpay api docs https://www.openpay.mx/docs/api/#cargo-en-banco
  """
  @enforce_keys [:method, :amount, :description]

  defstruct [:method, :amount, :description, :order_id, :due_date, :customer]
end
