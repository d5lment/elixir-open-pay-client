defmodule OpenpayClient.Models.Customer do
  @moduledoc """
    Customer definition from openpay api docs https://www.openpay.mx/docs/api/#clientes
  """

  defstruct [
    :id,
    :name,
    :last_name,
    :status,
    :balance,
    :clabe,
    :email,
    :phone_number,
    :creation_date,
    :external_id
  ]
end
