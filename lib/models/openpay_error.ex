defmodule OpenpayClient.Models.Error do
  @moduledoc """
    Error definition from openpay api docs https://www.openpay.mx/docs/api/#objeto-error
  """

  defstruct [
    :category,
    :error_code,
    :description,
    :http_code,
    :request_id,
    :fraud_rules
  ]
end
