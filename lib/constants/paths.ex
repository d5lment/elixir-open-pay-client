defmodule OpenpayClient.Paths do
  @moduledoc """
    Documentacion pendiente
  """

  @configuration Application.get_env(:openpay_client, :configuration)
  @merchant_id @configuration[:merchant_id]

  @customers_path "/#{@merchant_id}/customers/"
  @charges_path "/charges"
  @spei_pdf_path "https://sandbox-dashboard.openpay.mx/spei-pdf/#{@merchant_id}/"

  def get_customer_path, do: @customers_path

  def get_get_customer_path(customer_id), do: @customers_path <> customer_id

  def get_create_charge_in_bank_path(customer_id),
    do: @customers_path <> customer_id <> @charges_path

  def get_spei_pdf_path(transaction_id), do: @spei_pdf_path <> transaction_id
end
