defmodule OpenpayClient.Services.ChargeService do
  @moduledoc """
    Services related to customers
  """
  @behaviour OpenpayClient.Services.Behaviours.ChargeBehaviour

  alias OpenpayClient
  alias OpenpayClient.Paths
  alias OpenpayClient.Models.Transaction
  alias OpenpayClient.Handlers.StatusCodeHandler
  alias OpenpayClient.Models.Error

  def create_charge_in_bank(customer_id, request_charge_in_bank) do
    case OpenpayClient.post_request(
           Paths.get_create_charge_in_bank_path(customer_id),
           request_charge_in_bank
         ) do
      {:ok, response} ->
        body = StatusCodeHandler.handle_status_code(response)

        case body do
          {:error, %Error{}} = body ->
            body

          _ ->
            transaction = Poison.decode!(body, as: %Transaction{})
            {:ok, transaction}
        end

      {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
        {:error, "Service get_customer resolve: timeout"}

      _ ->
        {:error, "Error when call service"}
    end
  end
end
