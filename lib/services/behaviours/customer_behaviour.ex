defmodule OpenpayClient.Services.Behaviours.CustomerBehaviour do
  @moduledoc """
    Interface to services related with openpay customer.
  """
  alias OpenpayClient.Models.Customer
  alias OpenpayClient.Models.Requests.RequestCreateCustomer

  @type request_create_customer :: RequestCreateCustomer.t()
  @type customer :: Customer.t()
  @type customer_id :: integer
  @type openpay_error :: Error.t()

  @callback get_customer(customer_id) :: {:ok, Customer.t()} | {:error, Error.t()}

  @callback create_customer(request_create_customer) :: {:ok, Customer.t()} | {:error, Error.t()}
end
