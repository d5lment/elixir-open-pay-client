defmodule OpenpayClient.Services.Behaviours.ChargeBehaviour do
  @moduledoc """
    Interface to services related with charges.
  """
  alias OpenpayClient.Models.Transaction
  alias OpenpayClient.Models.Requests.RequestChargeInBank

  @type request_charge_in_bank :: RequestChargeInBank.t()
  @type customer_id :: integer
  @type customer :: Transaction.t()
  @type openpay_error :: Error.t()

  @callback create_charge_in_bank(customer_id, request_charge_in_bank) ::
              {:ok, Transaction.t()} | {:error, Error.t()}
end
