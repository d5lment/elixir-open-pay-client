defmodule OpenpayClient.Services.CustomerService do
  @moduledoc """
    Services related to customers
  """
  @behaviour OpenpayClient.Services.Behaviours.CustomerBehaviour

  alias OpenpayClient
  alias OpenpayClient.Paths
  alias OpenpayClient.Models.Customer
  alias OpenpayClient.Handlers.StatusCodeHandler
  alias OpenpayClient.Models.Error

  def create_customer(request_create_customer) do
    case OpenpayClient.post_request(Paths.get_customer_path(), request_create_customer) do
      {:ok, response} ->
        body = StatusCodeHandler.handle_status_code(response)

        case body do
          {:error, %Error{}} = body ->
            body

          _ ->
            customer = Poison.decode!(body, as: %Customer{})
            {:ok, customer}
        end

      {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
        {:error, "Service get_customer resolve: timeout"}

      _ ->
        {:error, "Error when call service"}
    end
  end

  def get_customer(customer_id) do
    case OpenpayClient.get_request(Paths.get_get_customer_path(customer_id)) do
      {:ok, response} ->
        body = StatusCodeHandler.handle_status_code(response)

        case body do
          {:error, %Error{}} = body ->
            body

          _ ->
            customer = Poison.decode!(body, as: %Customer{})
            {:ok, customer}
        end

      {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
        {:error, "Service get_customer resolve: timeout"}

      _ ->
        {:error, "Error when call service"}
    end
  end
end
