defmodule OpenpayClient.Handlers.ErrorHandler do
  @moduledoc """
    Throws an OpenpayClient.Exception when openpay operation fails
  """

  require Logger
  alias OpenpayClient.Models.Error

  def create_error(response) do
    error = get_openpay_detail_error(response)
    {:error, error}
  end

  defp get_openpay_detail_error(response) do
    case response.body do
      "" ->
        %{}

      _ ->
        Poison.decode!(response.body, as: %Error{})
    end
  end
end
