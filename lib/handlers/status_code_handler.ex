defmodule OpenpayClient.Handlers.StatusCodeHandler do
  @moduledoc """
    Manages the differents status codes from openpay https://www.openpay.mx/docs/api/#c-digos-de-error
  """
  @ok_status 200
  @created_status 201

  alias OpenpayClient.Handlers.ErrorHandler

  def handle_status_code(response) do
    case response.status_code do
      @ok_status ->
        response.body

      @created_status ->
        response.body

      _ ->
        ErrorHandler.create_error(response)
    end
  end
end
