defmodule OpenpayClient do
  @moduledoc """
  Documentation for OpenpayClient.
  """

  import OpenpayClient.Wrapper

  def get_request(url) do
    get(url, [], hackney_params())
  end

  def get_request(url, params) do
    params = Map.merge(hackney_params(), params)
    get(url, [], params)
  end

  def post_request(url) do
    post(url, [], [], hackney_params())
  end

  def post_request(url, params) when is_binary(params) do
    post(
      url,
      params,
      [],
      hackney_params()
    )
  end

  def post_request(url, params) do
    post(
      url,
      Poison.encode!(params),
      ["Content-Type": "application/json"],
      hackney_params()
    )
  end

  def delete_request(url) do
    delete(url, [], hackney_params())
  end

  def put_request(url, params) do
    put(url, Poison.encode!(params), [], hackney_params())
  end

  defp hackney_params do
    config = Application.get_env(:openpay_client, :configuration)
    [hackney: [basic_auth: {config[:user], config[:password]}]]
  end

  def ok({:ok, value}), do: value
end
